Title: Change default password hashing from sha512 to yescrypt
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2022-01-28
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-libs/pam[<1.5.2]

With sys-libs/pam[>=1.5.2] the default password hashing algorithm was
changed from sha512 to yescrypt [1]. As long as you don't set a new password
nothing will happen. Only newly set or changed passwords will be encrypted
with yescrypt.
Make sure you have at least sys-libs/pam[>=1.4.0], sys-apps/shadow[>=4.9]
and dev-libs/libxcrypt[>=4.3] installed before you update any passwords
though, otherwise you may end up locked out of your system.

You can check which users still have a sha512 password hash with

$ fgrep '$6$' /etc/shadow

or even a md5 hash

$ fgrep '$1$' /etc/shadow

If you don't want this change you can manually edit /etc/pam.d/system-auth to
your liking.

[1] https://www.openwall.com/yescrypt/
