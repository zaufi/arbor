# Copyright 2008 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gdb-6.7.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require flag-o-matic

export_exlib_phases src_configure src_test_expensive src_install

SUMMARY="The GNU Project Debugger"
HOMEPAGE="https://sourceware.org/${PN}"
DOWNLOADS="mirror://sourceware/${PN}/releases/${PNV}.tar.xz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation/ [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/download/ANNOUNCEMENT [[ lang = en ]]"

LICENCES="GPL-3 LGPL-3"
SLOT="0"
MYOPTIONS="
    debuginfod [[ description = [ Support for debuginfod, a HTTP server distributing debug info ] ]]
    guile [[ description = [ Extend gdb with guile, an implementation of the Scheme language ] ]]
    python
    source-highlight [[ description = [ Changes gdb to highlight source using GNU Source Highlight ] ]]
    zstd
    ( linguas:
        da de es fi fr ga id it ja nl pt_BR ro ru rw sv tr uk vi zh_CN
    )
"

RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/texinfo[>=6.3]
        sys-devel/gettext
        virtual/pkg-config
    test-expensive:
        dev-util/dejagnu
    build+run:
        dev-libs/expat
        dev-libs/gmp:=[>=4.3.2]
        dev-libs/mpc:=[>=0.8.1]
        dev-libs/mpfr:=[>=3.1.6]
        dev-libs/xxHash [[ note = [ Optional, but tiny and apparently fast ] ]]
        sys-libs/ncurses[>=5.2]
        sys-libs/readline:=[>=7.0]
        debuginfod? ( dev-util/elfutils[>=0.179][debuginfod] )
        guile? ( dev-lang/guile:=[>=2.0] )
        python? ( dev-lang/python:=[>=3] )
        source-highlight? ( app-text/source-highlight )
        zstd? ( app-arch/zstd )
        !sys-devel/binutils[<=2.25] [[
            description = [ gdb and binutils both installed libbfd and libiberty ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec-prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --enable-nls
    --enable-tui
    --disable-gold
    --disable-gprofng
    --disable-libbacktrace
    --disable-objc-gc
    --disable-pgo-build
    --disable-werror
    --with-curses
    --with-expat
    --with-system-readline
    --with-system-zlib
    --without-amd-dbgapi
    # These are already installed by binutils:
    --disable-install-libbfd
    --disable-install-libiberty
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    source-highlight
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    debuginfod
    guile
    python
    zstd
)

DEFAULT_SRC_COMPILE_PARAMS=(
    pkg_config_prog_path=/usr/$(exhost --target)/bin/$(exhost --tool-prefix)pkg-config

    # The check doesn't work properly for musl
    gt_cv_func_gnugettext_libc=yes
    gt_cv_func_gnugettext2_libc=yes
)

DEFAULT_SRC_INSTALL_EXTRA_SUBDIRS=( gdb sim )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( CONTRIBUTE )

gdb_src_configure() {
    CC_FOR_BUILD=$(exhost --build)-cc \
    CFLAGS_FOR_BUILD=$(print-build-flags CFLAGS) \
        default
}

gdb_src_test_expensive() {
    if [[ -n "${PALUDIS_DO_NOTHING_SANDBOXY}" ]]; then
        # make --dry-run check fails
        emake check
    else
        elog "Not running tests because gdb doesn't work under sydbox"
        elog "set PALUDIS_DO_NOTHING_SANDBOXY=1 if you want to run the tests"
    fi
}

gdb_src_install() {
    default

    # sys-devel/binutils installs opcodes.mo, bfd.mo, bfd.info, ctf-spec-info
    # and sframe-spec.info
    edo find "${IMAGE}" -name opcodes.mo -delete
    edo find "${IMAGE}" -name bfd.mo -delete
    edo rm "${IMAGE}"/usr/share/info/{bfd,ctf-spec,sframe-spec}.info

    edo find "${IMAGE}" -depth -type d -empty -delete
}

