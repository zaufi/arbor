# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ninja-build tag=v${PV} ]
require cmake
require elisp [ with_opt=true source_directory="../${PNV}/misc" ]
require bash-completion zsh-completion

export_exlib_phases pkg_pretend src_prepare src_compile src_install

SUMMARY="Small build system with a focus on speed"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="bash-completion vim-syntax zsh-completion"

# FIXME: need ninja installed for the native host when cross-compiling
# Proper testing utilizing dev-cpp/gtest is only possible when building with CMake, see:
# https://github.com/ninja-build/ninja/commit/4b6a8ac3b1a05da1a2ed7e4a4a0216c5b770bc74
DEPENDENCIES="
    build:
        app-doc/asciidoc
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
    run:
        dev-lang/python:*[>=3]
    test:
        dev-cpp/gtest
        dev-lang/python:*[>=3]
"
# NOTE: There's an automagic build dep on dev-util/re2c[>=0.15.3]to regenerate
#       two files which are shipped with the tarball anyway.

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR:PATH="/usr/share/doc/${PNVR}"
    -DNINJA_BUILD_BINARY:BOOL=TRUE
    -DNINJA_BUILD_DOC:BOOL=TRUE
    -DNINJA_PYTHON:STRING="python3"
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)


ninja_pkg_pretend() {
    if ! exhost --is-native -q && [[ ! -x /usr/$(exhost --build)/bin/ninja ]] ; then
        ewarn "You need to install ${PN} for $(exhost --build) if "
        ewarn "you want to build ${PN} for $(exhost --target)"
        die "Native ${PN} is required to cross-compile ${PN}"
    fi
}

ninja_src_prepare() {
    cmake_src_prepare

    # Fix for syd-3, see sydbox#54 for context.
    # Change the command "ls /" to "ls /proc" to make Landlock happy.
    # Note this directory must not be empty for the test to work,
    # hence /proc not /tmp as the latter may be a private tmpfs with syd-3.
    edo sed -i \
        -e '/kSimpleCommand\s\+=/s/\/"/\/proc"/' \
        ./src/subprocess_test.cc
}

ninja_src_compile() {
    cmake_src_compile

    elisp_src_compile
}

ninja_src_install() {
    cmake_src_install

    edo pushd "${CMAKE_SOURCE}"

    if option bash-completion; then
        dobashcompletion misc/bash-completion ninja
    fi

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax/
        doins misc/ninja.vim
    fi

    if option zsh-completion; then
        dozshcompletion misc/zsh-completion _ninja
    fi

    elisp_src_install

    edo popd
}

