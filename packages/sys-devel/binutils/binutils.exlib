# Copyright 2007 Bryan Østergaard
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] alternatives flag-o-matic

export_exlib_phases src_prepare src_configure src_compile src_test_expensive src_install

SUMMARY="Collection of binary tools including ld and as"

LICENCES="GPL-3 LGPL-3"
SLOT="0"
CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    aarch64-unknown-linux-musleabi
    armv4-unknown-linux-gnueabi
    armv4-unknown-linux-gnueabihf
    armv4-unknown-linux-musleabi
    armv4-unknown-linux-musleabihf
    armv5-unknown-linux-gnueabi
    armv5-unknown-linux-gnueabihf
    armv5-unknown-linux-musleabi
    armv5-unknown-linux-musleabihf
    armv6-unknown-linux-gnueabi
    armv6-unknown-linux-gnueabihf
    armv6-unknown-linux-musleabi
    armv6-unknown-linux-musleabihf
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-musleabi
    armv7-unknown-linux-gnueabihf
    armv7-unknown-linux-musleabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    i686-unknown-windows-gnu
    ia64-unknown-linux-gnu
    powerpc64-unknown-linux-gnu
    riscv32-unknown-linux-gnu
    riscv64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS="
    debuginfod [[ description = [
        Allows readelf/objectdump to query a debuginfod server for debug symbols ]
    ]]
    zstd [[ description = [ Support zstd compressed debug sections ] ]]
    zstd-by-default [[
        description = [ Use zstd as default compression for debug sections (use with caution) ]
        requires = zstd
    ]]

    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gnuconfig[>=20160211-r1]
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        !sys-devel/gdb[<=7.8.1] [[
            description = [ gdb and binutils both installed libbfd and libiberty ]
            resolution = uninstall-blocked-after
        ]]
        debuginfod? ( dev-util/elfutils[>=0.179][debuginfod] )
        zstd? ( app-arch/zstd[>=1.4.0] )
    test-expensive:
        dev-tcl/expect
        sys-apps/bc
"

if ever at_least 2.42 ; then
    DEPENDENCIES+="
        build:
            sys-devel/gettext
        build+run:
            debuginfod? ( dev-util/elfutils[>=0.188][debuginfod] )
    "
fi

ECONF_SOURCE="${WORK}"

binutils_src_prepare() {
    if [[ -d ${FILES}/patches-${PV} ]]; then
        expatch -p1 "${FILES}"/patches-${PV}
    fi

    # fixup broken locale install paths
    edo sed -e '/^localedir = /clocaledir = /usr/share/locale'          \
            -e '/^gnulocaledir = /cgnulocaledir = /usr/share/locale'    \
            -i "${WORK}/bfd/po/Make-in"                                 \
            -i "${WORK}/binutils/po/Make-in"                            \
            -i "${WORK}/gas/po/Make-in"                                 \
            -i "${WORK}/gold/po/Make-in"                                \
            -i "${WORK}/gprof/po/Make-in"                               \
            -i "${WORK}/ld/po/Make-in"                                  \
            -i "${WORK}/opcodes/po/Make-in"

    default
}

target_supports_gold() {
    case "${1}" in
    # Exclude non-ELF targets.
    *-*-windows*) return 1 ;;
    # Check for target supported by gold.
    aarch64*-*-* | arm*-*-* | i?86-*-* | powerpc*-*-* | x86_64-*-*) return 0 ;;
    *) return 1 ;;
    esac
}

binutils_src_configure() {
    local host=$(exhost --target) target=

    export CC_FOR_BUILD=$(exhost --build)-cc
    export CFLAGS_FOR_BUILD=$(print-build-flags CFLAGS)
    export LDFLAGS_FOR_BUILD=$(print-build-flags LDFLAGS)

    for target in ${CROSS_COMPILE_TARGETS} ; do
        local libpath=/${target}/lib:/usr/${target}/lib

        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"

        edo mkdir -p "${WORKBASE}/build/${target}"
        edo cd "${WORKBASE}/build/${target}"

        # NOTE(compnerd) explicitly disable gdb incase we are building from scm.  Upstream bundles
        # gdb and binutils and this will otherwise build gdb which is packaged separately.
        local myconf=( --target=${target} --disable-werror --disable-gdb --enable-ld --enable-lto )
        if target_supports_gold "${target}" ; then
            myconf+=( --enable-gold )
        fi

        if [[ ${target} == i686-*-linux-* ]] || [[ ${target} == x86_64-*-linux-* ]] ; then
            myconf+=( --enable-cet )
        else
            myconf+=( --disable-cet )
        fi

        myconf+=(
            --enable-compressed-debug-sections=all
            --enable-libctf
            --with-system-zlib
            $(option_with debuginfod)
        )

        # NOTE(compnerd) BFD options
        myconf+=( --enable-plugins --enable-64-bit-bfd --enable-secureplt --with-mmap )
        # TODO(compnerd) determine the debug location from paludis and pass that along
        # myconf+=( --wih-separate-debug-dir=/usr/${target}/lib/debug )
        myconf+=( --with-system-zlib )

        # NOTE(compnerd) binutils options
        myconf+=(
            --enable-colored-disassembly
            --disable-gprofng
            # "Used to validate --package-metadata= input. Disabled by default."
            --disable-jansson
            --with-system-zlib
            # Allows to dump an elf note (NT_AMDGPU_METADATA) for AMDGPUs
            # https://llvm.org/docs/AMDGPUUsage.html#code-object-v3-and-above-note-records
            --without-msgpack
            $(option_with zstd)
            $(option zstd-by-default --enable-default-compressed-debug-sections-algorithm=zstd --enable-default-compressed-debug-sections-algorithm=zlib)
        )

        # TODO(compnerd) disable installation of libiberty and libbfd
        # TODO(compnerd) this is not the default behaviour, determine why so we can nix it
        myconf+=( --enable-shared --enable-install-libbfd --enable-install-libiberty )

        # NOTE(compnerd) gas options
        myconf+=( --with-system-zlib )
        # NOTE(compnerd) glibc has supported this since 2007, if you have an older glibc, we should
        # conditionalise this on the libc target version
        myconf+=( --enable-elf-stt-common )

        # NOTE(compnerd) gold options
        myconf+=( --with-sysroot='""' --enable-threads --enable-plugins --with-lib-path=${libpath} )

        # NOTE(compnerd) ld options
        myconf+=( --with-lib-path=${libpath} --enable-64-bit-bfd --with-sysroot='""' )

        # NOTE(somasis) default to creating archives deterministically; good for reproducible building
        myconf+=( --enable-deterministic-archives )

        # NOTE(tgurr) default to DT_RUNPATH as the use of DT_RPATH is deprecated (see man ld.so)
        myconf+=( --enable-new-dtags )

        [[ "${target}" == "${host}" ]] && myconf+=( --program-prefix=${host}- )

        econf "${myconf[@]}"
    done
}

binutils_src_compile() {
    local target=

    for target in ${CROSS_COMPILE_TARGETS} ; do
        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"
        edo cd "${WORKBASE}/build/${target}"
        default
    done
}

binutils_src_test_expensive() {
    local target=

    for target in ${CROSS_COMPILE_TARGETS} ; do
        if option !targets:${target} ; then
            echo "    Cross-Compile Target; ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"
        edo cd "${WORKBASE}/build/${target}"
        emake "${DEFAULT_SRC_TEST_PARAMS[@]}" check
    done
}

linkers_for_target() {
    if target_supports_gold "${1}" ; then
        echo bfd gold
    else
        echo bfd
    fi
}

binutils_src_install() {
    local host=$(exhost --target) target=

    for target in ${CROSS_COMPILE_TARGETS} ; do
        local m symbols
        local alternatives=()
        local as_alternatives=()
        # eclectic managed files
        local em=( /usr/${host}/bin/${target}-ld /usr/${host}/${target}/bin/ld )
        local man_dir=/usr/share/man/man1/

        if option !targets:${target} ; then
            echo "    Cross-Compile Target: ${target} (disabled)"
            continue
        fi

        echo "    Cross-Compile Target: ${target}"

        edo cd "${WORKBASE}/build/${target}"

        # default installation
        default

        # don't install everything twice
        for f in $(find "${IMAGE}"/usr/${host}/${target}/bin/ -type f ); do
            edo rm "${f}"
        done

        edo mkdir -p "${IMAGE}"/usr/${host}/${target}/bin
        for provider in $(linkers_for_target ${target}); do
            edo mv "${IMAGE}"/usr/${host}/bin/${target}-ld.${provider} "${IMAGE}"/usr/${host}/${target}/bin/ld.${provider}
            dosym ../${target}/bin/ld.${provider} /usr/${host}/bin/${target}-ld.${provider}
        done
        edo rm "${IMAGE}"/usr/${host}/bin/${target}-ld

        edo mv "${IMAGE}"/usr/${host}/{bin/${target}-as,${target}/bin/as.${PN}}
        edo mv "${IMAGE}"/${man_dir}/{${target}-as.1,${target}-as.${PN}.1}
        as_alternatives=(
            assembler binutils 100
            /usr/${host}/${target}/bin/as /usr/${host}/${target}/bin/as.${PN}
            /usr/${host}/bin/${target}-as /usr/${host}/${target}/bin/as.${PN}
            ${man_dir}/${target}-as.1 ${man_dir}/${target}-as.${PN}.1
        )
        if [[ ${target} == ${host} ]]; then
            dobanned as
            as_alternatives+=(
                /usr/${host}/bin/as /usr/${host}/${target}/bin/as.${PN}
                ${man_dir}/as.1 ${man_dir}/${target}-as.${PN}.1
                "${BANNEDDIR}"/as as.${PN}
            )
        fi

        alternatives=( compiler-tools binutils 100 )
        for f in $(find "${IMAGE}"/usr/${host}/bin/ -type f ); do
            local name=${f##*/}
            name=${f##*-}
            local target_suffix=".${PN}"
            local t=/usr/${host}/${target}/bin/${name}${target_suffix}
            edo mv "${f}" "${IMAGE}"/${t}

            alternatives+=( /usr/${host}/${target}/bin/${name} ${t} )
            alternatives+=( /usr/${host}/bin/${target}-${name} ${t} )

            if [[ ${target} == ${host} ]]; then
                # Add unprefixed tools
                alternatives+=( /usr/${host}/bin/${name} ${t} )
                dobanned ${name}
                alternatives+=( "${BANNEDDIR}"/${name} ${name}${target_suffix} )
            fi

            local src_man=${man_dir}/${target}-${name}.1
            local man=${man_dir}/${target}-${name}.${PN}.1
            if [[ -f "${IMAGE}"/${src_man} ]]; then
                mv "${IMAGE}"/{${src_man},${man}}
                alternatives+=( ${src_man} ${man} )

                if [[ ${target} == ${host} ]]; then
                    # Add unprefixed man pages
                    alternatives+=( ${man_dir}/${name}.1 ${man} )
                fi
            fi
        done
        alternatives_for "${alternatives[@]}"

        alternatives_for "${as_alternatives[@]}"

        # alternatives setup
        for provider in $(linkers_for_target ${target}) ; do
            local priority=

            case "${provider}" in
            bfd)  priority=100 ;;
            gold) priority=10  ;;
            esac

            local em_ld=/usr/${host}/${target}/bin/ld.${provider}
            alternatives=( ld ${provider} ${priority} )
            for m in "${em[@]}" ; do
                alternatives+=( "${m}" ${em_ld} )
            done
            alternatives+=( ${man_dir}/${target}-ld.1 ${target}-ld.${PN}.1 )
            if [[ ${target} == ${host} ]]; then
                alternatives+=( /usr/${host}/bin/ld ${em_ld} )
                dobanned ld.${provider}
                alternatives+=( "${BANNEDDIR}"/ld ld.${provider} )
                alternatives+=( ${man_dir}/ld.1 ${target}-ld.${PN}.1 )
            fi
            alternatives_for "${alternatives[@]}"
        done
    done
}

