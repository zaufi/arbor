# Copyright 2007 David Leverton <dleverton@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'screen-4.0.3.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require gnu [ suffix=tar.gz ]
require pam systemd-service autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]

export_exlib_phases src_install

SUMMARY="A full-screen terminal multiplexer/window manager"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    setuid [[ description = [ Install screen setuid root, allowing you to share screen sessions between multiple users ] ]]

    ( libc: musl )
"

# fail to build, last checked: 5.0.0
RESTRICT="test"

DEPENDENCIES="
    build+run:
        !setuid? ( group/utmp )
        sys-libs/ncurses
        sys-libs/pam
        !libc:musl? ( dev-libs/libxcrypt:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-5.0.0-no-utempter.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-pam
    --enable-socket-dir=/run/screen
    --enable-telnet
    --with-system_screenrc=/etc/screenrc
)

screen_src_install() {
    default

    insinto /etc
    newins etc/etcscreenrc screenrc

    if ! option setuid ; then
        edo chown root:utmp "${IMAGE}"/usr/$(exhost --target)/bin/${PN}
        edo chmod 2755 "${IMAGE}"/usr/$(exhost --target)/bin/${PN}
        insinto ${SYSTEMDTMPFILESDIR}
        hereins ${PN}.conf <<EOF
d /run/screen 0775 root utmp
EOF
    else
        edo chown root:root "${IMAGE}"/usr/$(exhost --target)/bin/${PN}
        edo chmod 4755 "${IMAGE}"/usr/$(exhost --target)/bin/${PN}
        insinto ${SYSTEMDTMPFILESDIR}
        hereins ${PN}.conf <<EOF
d /run/screen 0755 root root
EOF
    fi

    pamd_mimic_system screen auth auth
}

