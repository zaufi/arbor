# Copyright 2023-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenPrinting release=${PV} suffix=tar.xz ] \
    systemd-service [ systemd_files=[ "${WORK}"/daemon/cups-browsed.service ] ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]

SUMMARY="Daemon to browse the network for remote CUPS queues and IPP printers"
DESCRIPTION="
This package contains cups-browsed, a helper daemon to browse the network for remote CUPS queues
and IPP network printers and automatically create local queues pointing to them.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    avahi
    ldap
"

RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.30.2]
        dev-libs/libcupsfilters[>=2.0.0]
        dev-libs/libppd[>=2.0.0]
        net-print/cups[>=2.2.2]
        avahi? ( net-dns/avahi )
        ldap? ( net-directory/openldap )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/1debe6b140c37e0aa928559add4abcc95ce54aa2.patch
)

src_configure() {
    local myconf=(
        --enable-frequent-netif-update
        --enable-nls
        --disable-auto-setup-driverless-only
        --disable-auto-setup-local-only
        --disable-saving-created-queues
        --disable-static
        --disable-werror
        --with-browseremoteprotocols="$(option avahi && echo 'dnssd ')$(option ldap && echo 'ldap ')"
        --with-cups-config=/usr/$(exhost --target)/bin/cups-config
        --with-cups-domainsocket=/run/cups/cups.sock
        --with-cups-rundir=/run/cups
        --with-remote-cups-local-queue-naming=RemoteName
        --without-rcdir
        $(option_enable avahi)
        $(option_enable ldap)
    )

    econf "${myconf[@]}"
}

src_install() {
    default

    install_systemd_files

    if ! option avahi ; then
        edo sed -e 's: avahi-daemon.service::g' \
                -i "${IMAGE}"/usr/$(exhost --target)/lib/systemd/system/cups-browsed.service
    fi
}

