# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release="v${PV}" suffix=tar.xz ]

SUMMARY="An implementation of the Hypertext Transfer Protocol version 2 in C"
HOMEPAGE+=" https://nghttp2.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    debug examples systemd utils

    systemd [[ requires = utils ]]
    utils? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"
#doc - Currently broken with the bundled sphinxcontrib.rubydomain: "Could not
# import extension sphinxcontrib.rubydomain (exception: No module named
# rubydomain)

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/libxml2:2.0[>=2.6.26]
        examples? ( dev-libs/libevent:=[>=2.0.8] )
        systemd? ( sys-apps/systemd[>=209] )
        utils? (
            dev-libs/boost[>=1.54.0]
            dev-libs/jansson[>=2.5]
            dev-libs/jemalloc
            dev-libs/libev[>=4.15]
            net-dns/c-ares[>=1.16.0]
            sys-libs/zlib[>=1.2.3]
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
        )
"
#        doc? ( dev-python/Sphinx[python_abis:*(-)?] )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-http3
    --disable-static
    --disable-werror
    --with-libxml2
    --without-libbrotlidec
    --without-libbrotlienc
    --without-libbpf
    --without-libnghttp3
    --without-libngtcp2
    --without-wolfssl
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    examples
    'utils app'
    'utils hpack-tools'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'examples libevent-openssl'
    systemd
    'utils jansson'
    'utils jemalloc'
    'utils libcares'
    'utils libev'
    'utils openssl'
    'utils zlib'
)

src_compile() {
    default

    # option doc && emake -C doc html
}

src_install() {
    default

    if ! option utils; then
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
        edo rm -r "${IMAGE}"/usr/share/man
    fi

    # option doc && dodoc -r doc/manual/html/*
}

