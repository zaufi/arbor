# Copyright 2019-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=pybind tag=v${PV} ] cmake \
    setup-py [ import=setuptools blacklist=2 ]

SUMMARY="Lightweight header-only library that exposes C++ types in Python"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# broken, last checked: 2.10.3
RESTRICT="test"

DEPENDENCIES="
    test:
        dev-cpp/catch
        dev-libs/boost
        dev-python/pytest[python_abis:*(-)?]
        sci-libs/eigen:3
"

src_unpack() {
    cmake_src_unpack
    easy-multibuild_src_unpack
}

src_prepare() {
    cmake_src_prepare
    setup-py_src_prepare

    # fix cmake & pkg-config install directories
    edo sed \
        -e 's:{CMAKE_INSTALL_DATAROOTDIR}/cmake:{CMAKE_INSTALL_LIBDIR}/cmake:g' \
        -e 's:{CMAKE_INSTALL_DATAROOTDIR}/pkgconfig:{CMAKE_INSTALL_LIBDIR}/pkgconfig:g' \
        -i CMakeLists.txt
}

src_configure() {
    local cmakeparams=(
        -DBUILD_TESTING:BOOL=$(expecting_tests TRUE FALSE)
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DDOWNLOAD_CATCH:BOOL=FALSE
        -DDOWNLOAD_EIGEN:BOOL=FALSE
        -DPYBIND11_CUDA_TESTS:BOOL=FALSE
        -DPYBIND11_FINDPYTHON:BOOL=FALSE
        -DPYBIND11_INSTALL:BOOL=TRUE
        -DPYBIND11_NOPYTHON:BOOL=FALSE
        -DPYBIND11_NUMPY_1_ONLY:BOOL=FALSE
        -DPYBIND11_SIMPLE_GIL_MANAGEMENT:BOOL=FALSE
        -DPYBIND11_USE_CROSSCOMPILING:BOOL=FALSE
        -DPYBIND11_WERROR:BOOL=FALSE
        -DPYTHON_EXECUTABLE:PATH=${PYTHON}
    )

    ecmake \
        "${cmakeparams[@]}"

    setup-py_src_configure
}

src_compile() {
    ecmake_build
    setup-py_src_compile
}

src_test() {
    ectest
}

src_install() {
    cmake_src_install
    setup-py_src_install
}

