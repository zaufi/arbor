# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2013-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=$(ever replace_all _)
if [[ ${PV} == *_beta* ]]; then
    MY_PNV=${PN}_${MY_PV/beta/b}
    DOWNLOADS="https://boostorg.jfrog.io/artifactory/main/beta/$(ever range 1-3).beta1/source/${MY_PNV}.tar.bz2"
    WORK=${WORKBASE}/${PN}_${MY_PV%*_beta*}
else
    MY_PNV=${PN}_${MY_PV}
    DOWNLOADS="https://boostorg.jfrog.io/artifactory/main/release/${PV}/source/${MY_PNV}.tar.bz2"
    WORK=${WORKBASE}/${MY_PNV}
fi

require flag-o-matic toolchain-funcs
require python [ blacklist=2 multiunpack=false with_opt=true ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Boost Libraries for C++"
HOMEPAGE="https://www.${PN}.org"

UPSTREAM_CHANGELOG="${HOMEPAGE}/users/history/version_${PV//./_}.html [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc/libs/${PV//./_} [[ lang = en ]]"

LICENCES="Boost-1.0"
SLOT="0"
MYOPTIONS="debug doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
    build+run:
        app-arch/zstd [[ note = [ Used by Boost.Iostreams ] ]]
        dev-libs/expat
        dev-libs/icu:=
        python? (
"

for abi in ${PYTHON_FILTERED_ABIS};  do
    DEPENDENCIES+="python_abis:${abi}? ( dev-python/numpy:$(numpy_slot ${abi})[python_abis:${abi}] ) "
done

DEPENDENCIES+=" ) [[ *note = [ Automagic dependency of Boost.Python ] ]]"

if ever at_least 1.85.0 ; then
    DEPENDENCIES+="
        build+run:
            sys-libs/libquadmath:=
    "
fi

BUILD_DIR="${WORKBASE}"/build

global_conf=(
    -d+2
    -j${EXJOBS:-1}
    -q
    --build-dir="${BUILD_DIR}"
    --layout=system
    --without-mpi
    link=shared
    pch=off
    runtime-link=shared
    threading=multi
)

boost_src_prepare() {
    default

    # Trick the configure test to accept our prefixed compiler as a valid part
    # of the toolset.
    edo sed \
        -e "/test_toolset cxx &&/s/\(test_compiler\) cxx/\1 $(exhost --target)-c++/" \
        -e "/test_toolset cxx &&/s/\(test_compiler\) cpp/\1 $(exhost --target)-cpp/" \
        -e "/test_toolset cxx &&/s/\(test_compiler\) CC/\1 $(exhost --target)-cc/" \
        -i tools/build/src/engine/build.sh
}

boost_src_configure() {
    # boost's buildsystem runs python code which imports numpy in order
    # to figure out the correct include directory. This does not work
    # when cross-compiling, so we create a template which the multiple
    # `compile_one_multibuild` invocations can modify
    edo cp libs/python/build/Jamfile{,.template}
    edo sed -e "s:<include>\$(numpy-include):<include>@PYTHON_SITEDIR@/numpy/core/include:" \
            -i libs/python/build/Jamfile.template

    edo env \
    CXX=$(exhost --build)-c++ \
    CXXFLAGS="$(print-build-flags CXXFLAGS)" \
    ./bootstrap.sh \
        --with-icu=/usr/$(exhost --build) \
        --with-python=/usr/$(exhost --build)/bin/python3 \
        --with-python-root=/usr/$(exhost --build) \
        --with-python-version=3 \
        --with-toolset=cxx

    easy-multibuild_src_configure
}

_create_project_config() {
    local compiler compiler_version
    if cxx-is-gcc; then
        compiler=gcc
        compiler_version=$(gxx-fullversion)
    elif cxx-is-clang; then
        compiler=clang
        compiler_version=$(clangxx-fullversion)
    else
        die "Unsupported compiler detected"
    fi

    cat <<EOF > project-config.jam
using ${compiler} : ${compiler_version} : $(exhost --target)-c++ :
    <cflags>"${CFLAGS}"
    <cxxflags>"${CXXFLAGS}"
    <linkflags>"${LDFLAGS}"
    <archiver>"${AR}"
    <ranlib>"${RANLIB}"
    ;

using python
    : $(python_get_abi)
    : python$(python_get_abi)
    : $(python_get_incdir)
    : $(python_get_libdir)
    ;
EOF

    global_conf+=( toolset=${compiler}-${compiler_version} )
}

compile_one_multibuild() {
    edo pushd "${ECONF_SOURCE}"

    # Overwrite the config the bootstrap script created with our own
    # FIXME: need to do this here because of python version
    _create_project_config

    # Set ABI specific python path in order to pick up correct numpy headers
    edo cp libs/python/build/Jamfile{.template,}
    edo sed -e "s:@PYTHON_SITEDIR@:$(python_get_sitedir):" \
            -i libs/python/build/Jamfile

    edo ./b2 \
        "${global_conf[@]}" \
        $(optionq debug && echo variant=debug) \
        $(optionq !python && echo --without-python)

    # Move build artifacts out of the way so they don't get re-used by
    # the next compilation
    option python && edo mv "${BUILD_DIR}"/boost/bin.v2/libs/python{,-$(python_get_abi)}

    edo popd
}

boost_src_compile() {
    easy-multibuild_src_compile
}

install_one_multibuild() {
    edo pushd "${ECONF_SOURCE}"

    _create_project_config

    # Move artifacts for this ABI back into place
    edo pushd "${BUILD_DIR}"/boost
    edo rm -fr bin.v2/libs/python
    option python && edo mv bin.v2/libs/python{-$(python_get_abi),}
    edo popd

    # Careful: The IMAGE-prefixed paths *must* precede global_conf
    edo ./b2 \
        --prefix="${IMAGE}"/usr \
        --libdir="${IMAGE}"/usr/$(exhost --target)/lib \
        --includedir="${IMAGE}"/usr/$(exhost --target)/include \
        "${global_conf[@]}" \
        $(optionq debug && echo variant=debug) \
        $(optionq !python && echo --without-python) \
        install

    edo popd
}

boost_src_install() {
    local lib

    easy-multibuild_src_install

    # Install compatibility symlinks to get -mt-suffixed libs
    edo pushd "${IMAGE}"/usr/$(exhost --target)/lib
    for lib in *.so; do
        dosym ${lib} /usr/$(exhost --target)/lib/${lib/.so/-mt.so}
    done
    edo popd

    if option doc ; then
        # FIXME Handled by the build system originally but seems to be broken
        local f
        edo find doc/ libs/ -depth -iname 'test' -o -iname 'src' -o -iname 'CMakeLists.txt' -o -iname '*.cmake' -o -iname 'Jamfile.v2' -o -iname 'proj' -o -iname '*.vcproj' | while read f ; do
            edo rm -r "${f}"
        done

        insinto /usr/share/doc/${PNVR}/html
        doins -r doc libs more index.html boost.css boost.png

        dosym /usr/$(exhost --target)/include/${PN} /usr/share/doc/${PNVR}/html/${PN}
    fi

    if ! option python ; then
        edo rm -r "${IMAGE}"/usr/$(exhost --target)/include/boost/{python,python.hpp}
    fi

    # Remove empty dir
    option doc && edo rmdir "${IMAGE}"/usr/share/doc/${PNVR}/html/libs/json/cmake
}

