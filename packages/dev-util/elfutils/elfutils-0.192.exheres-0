# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

SUMMARY="Libraries/utilities to handle ELF objects"
HOMEPAGE="https://sourceware.org/${PN}"
DOWNLOADS="${HOMEPAGE}/ftp/${PV}/${PNV}.tar.bz2"

LICENCES="GPL-3 || ( GPL-2 LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    debuginfod [[
        description = [ Build debuginfod, a HTTP server distributing debug info ]
    ]]
    zstd
    ( libc: musl )
    ( linguas: de es ja pl uk )
"

RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex[>=2.5.4]
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib
        libc:musl? (
            dev-libs/argp-standalone
            dev-libs/musl-fts
            dev-libs/musl-obstack[>=1.1]
        )
        !dev-libs/libelf [[
            description = [ installs the same headers, but the libraries are ABI incompatible ]
            resolution = uninstall-blocked-before
        ]]
        debuginfod? (
            app-arch/libarchive[>=3.1.2]
            dev-db/sqlite:3[>=3.7.17]
            dev-libs/json-c:=[>=0.11]
            net-libs/libmicrohttpd[>=0.9.33]
            net-misc/curl[>=7.29.0]
        )
        zstd? ( app-arch/zstd[>=1.4.0] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Provide-missing-FNM_EXTMATCH-define-for-musl.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --program-prefix=eu-
    --enable-nls
    --disable-debuginfod-ima-verification
    --disable-sanitize-address
    --disable-sanitize-memory
    --disable-stacktrace
    --disable-valgrind
    --with-bzlib
    --with-lzma
    --with-zlib
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debuginfod
    'debuginfod libdebuginfod'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( zstd )

src_prepare() {
    default

    if [[ $(exhost --target) == *-musl* ]]; then
        edo cp "${FILES}"/error.h "${WORK}"/lib
        edo cp "${FILES}"/error.h "${WORK}"/src
    fi
}

src_configure() {
    # NOTE (abdulras) tests require that the debug information be present in the tools
    expecting_tests && append-flags -g

    # There are some warnings when building with musl
    append-flags -Wno-error

    default
}

src_install() {
    default

    # Drop-in dir for debuginfod urls (& ima certificates)
    keepdir /etc/debuginfod
}

