# Copyright 2024 Morgane “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam -b slotted=false
if exparam -b slotted; then
    require alternatives
fi

export_exlib_phases src_fetch_extra src_unpack src_install

DEPENDENCIES="
    build:
        dev-lang/node[>=14.19.0]
"

WORK=${WORKBASE}/${PN}/${PV}

COREPACK_ARCHIVE=${FETCHEDDIR}/corepack/${PNV}.tar.gz

read -r -d '' COREPACK_METADATA_READER <<EOF
let fs = require('fs');
let metadata = JSON.parse(fs.readFileSync('.corepack', 'utf8')).bin;
let [_, prefix, moduledir, list_file] = process.argv;

fs.writeFileSync(list_file, (
        Array.isArray(metadata)
        ? metadata.map(b => [ prefix + '/bin/' + b, moduledir + '/${PN}.js' ])
        : Object.entries(metadata).map(e => [ prefix + '/bin/' + e[0], moduledir + '/' + (e[1].startsWith('./') ? e[1].substring(2) : e[1]) ])
    ).flat().join('\n') + '\n'
);
EOF

corepack-package-manager_src_fetch_extra() {
    edo mkdir -p "${FETCHEDDIR}"/corepack
    edo corepack pack ${PN}@${PV} -o "${COREPACK_ARCHIVE}"
}

corepack-package-manager_src_unpack() {
    PALUDIS_UNPACK_ANY_PATH=yes unpack --if-compressed "${COREPACK_ARCHIVE}"
}

corepack-package-manager_dosym_list() {
    local one=${1}
    local two=${2}
    shift 2
    dosym "${one}" "${two}"
    if [[ ${#} -gt 0 ]]; then
        corepack-package-manager_dosym_list "${@}"
    fi
}

corepack-package-manager_src_install() {
    local prefix=/usr/$(exhost --target)
    local moduledir=${prefix}/lib/node_modules/${PNV}


    local binaries_files=${TEMP}/binaries.list
    edo node -e "${COREPACK_METADATA_READER}" ${prefix} ${moduledir} "${binaries_files}"

    # remove .cmd files if any
    edo find "${WORK}" -name "*.cmd" -exec rm {} \;

    # remove corepack metadata
    edo rm .corepack

    insinto ${moduledir}
    doins -r .

    local binaries
    readarray -t binaries < "${binaries_files}"

    local bin
    for bin in "${binaries[@]}"; do
        bin=${IMAGE}${bin}
        [[ -f "${bin}" ]] && edo chmod +x "${bin}"
    done

    if exparam -b slotted; then
        alternatives_for ${PN} ${SLOT} ${SLOT} \
            "${binaries[@]}" \
            "${COREPACK_PACKAGE_MANAGER_EXTRA_ALTERNATIVES[@]}"
    else
        corepack-package-manager_dosym_list "${binaries[@]}"
    fi
}

