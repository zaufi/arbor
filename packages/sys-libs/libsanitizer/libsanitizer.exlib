# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Based on the libatomic exheres which is
#   Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
#   Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries
require flag-o-matic

SUMMARY="GCC Sanitizer Libraries"

LICENCES="GPL-2"
SLOT="$(ever major)"

DEPENDENCIES="
    build:
        sys-libs/libstdc++:${SLOT}
"

MYOPTIONS=""

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/${PN}"
    WORK="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/build/$(exhost --target)/${PN}"
elif [[ ${PV} == *-rc* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/rc/RC-}/${PN}"
    WORK="${WORKBASE}/gcc-${PV/rc/RC-}/build/$(exhost --target)/${PN}"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/${PN}"
    WORK="${WORKBASE}/gcc-${PV/_p?(re)/-}/build/$(exhost --target)/${PN}"
fi

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-multilib )

if ever at_least 13.0.1_pre20230122 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --with-gcc-major-version-only
    )
fi

libsanitizer_src_unpack() {
    default
    edo mkdir -p "${WORK}"
}

libsanitizer_src_prepare() {
    #Force to use the already installed libstdc++.la for this target
    edo sed -i 's|\\\$(top_builddir)/../libstdc++-v3/src|/usr/'"$(exhost --target)"'/lib|' "${ECONF_SOURCE}/configure"
    edo cd "${ECONF_SOURCE}/.."
    default
}

libsanitizer_src_configure() {
    if ever at_least 12 ; then
        :
    else
        replace-flags -D_FORTIFY_SOURCE=3 -DFORTIFY_SOURCE=2
    fi

    CC=$(exhost --tool-prefix)gcc-${SLOT}       \
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}  \
    CXX=$(exhost --tool-prefix)g++-${SLOT}      \
    default
}

libsanitizer_src_install() {
    default

    if ever at_least 13 ; then
        symlink_dynamic_libs lib{a,hwa,l,t,ub}san
        slot_dynamic_libs lib{a,hwa,l,t,ub}san
        slot_other_libs lib{a,hwa,l,t,ub}san.{a,la}
        slot_other_libs lib{a,hwa,l,t,ub}san_preinit.o
    else
        symlink_dynamic_libs lib{a,l,t,ub}san
        slot_dynamic_libs lib{a,l,t,ub}san
        slot_other_libs lib{a,l,t,ub}san.{a,la}
        slot_other_libs lib{a,l,t,ub}san_preinit.o
    fi
    slot_other_libs ${PN}.spec

}

export_exlib_phases src_unpack src_prepare src_configure src_install

