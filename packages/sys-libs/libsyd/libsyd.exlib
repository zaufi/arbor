# Copyright 2023 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    SCM_BRANCH=main
    SCM_REPOSITORY="https://git.sr.ht/~alip/syd"
    require scm-git
fi

require cargo [ debug=true rust_minimum_version=1.65.0 ]
require elisp [ with_opt=true source_directory=src ]
require python [ blacklist=2 has_bin=false has_lib=true multibuild=false with_opt=true ]
require ruby [ multibuild=false with_opt=true ]
require rust

SUMMARY="Library of The ☮ther SⒶndbøx"
DESCRIPTION="Rust-based C library for SydB☮x interaction via /dev/syd"
SLOT="3"
MYOPTIONS="emacs perl python ruby"

if ! ever is_scm; then
    DOWNLOADS="https://git.sr.ht/~alip/syd/archive/${PN}-${PV}.tar.gz -> ${PNV}.tar.gz"
    WORK="${WORKBASE}/syd-${PN}-${PV}/lib"
else
    WORK="${WORK}/lib"
fi

DEPENDENCIES="
    build+run:
        perl? (
            dev-lang/perl:=
            dev-perl/FFI-CheckLib
            dev-perl/FFI-Platypus
            dev-perl/JSON
            dev-perl/Math-Int64
        )
        ruby? (
               dev-ruby/ffi[ruby_abis:*(-)?]
        )
"

export_exlib_phases src_compile src_install src_test

libsyd_src_compile() {
    cargo_src_compile
    elisp_src_compile
}

libsyd_src_install() {
    local target=./target/$(rust_target_arch_name)/release
    dolib "${target}"/libsyd.so
    dolib.a "${target}"/libsyd.a

    insinto /usr/$(exhost --target)/include
    doins syd.h

    if option perl; then
        sitedir=$(perl -MConfig -e 'print $Config{installsitelib}')
        insinto "${sitedir}"
        doins src/syd.pm
    fi

    if option python; then
        insinto $(python_get_sitedir)
        doins src/syd.py
    fi

    if option ruby; then
        insinto /usr/$(exhost --target)/lib/ruby/$(ruby_get_abi)
        doins src/syd.rb
    fi

    elisp_src_install

    emagicdocs
}

libsyd_src_test() {
    if ! esandbox check 2>/dev/null; then
        option perl && cargo_src_test pl
        option python && cargo_src_test py
        option ruby && cargo_src_test rb
    else
        elog "Not running tests because SydB☮x doesn't work under SydB☮x"
    fi
}
