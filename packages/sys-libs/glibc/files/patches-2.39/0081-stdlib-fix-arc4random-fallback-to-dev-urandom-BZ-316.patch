Upstream: yes, taken from release/2.39/master

From a03631124602f2dcef40d46660b96d2e51c44bfd Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Fri, 5 Apr 2024 10:27:29 -0300
Subject: [PATCH 81/86] stdlib: fix arc4random fallback to /dev/urandom (BZ
 31612)

The __getrandom_nocancel used by __arc4random_buf uses
INLINE_SYSCALL_CALL (which returns -1/errno) and the loop checks for
the return value instead of errno to fallback to /dev/urandom.

The malloc code now uses __getrandom_nocancel_nostatus, which uses
INTERNAL_SYSCALL_CALL, so there is no need to use the variant that does
not set errno (BZ#29624).

Checked on x86_64-linux-gnu.

Reviewed-by: Xi Ruoyao <xry111@xry111.site>
(cherry picked from commit 184b9e530e6326e668709826903b6d30dc6cac3f)
---
 NEWS                | 2 ++
 stdlib/arc4random.c | 2 +-
 2 files changed, 3 insertions(+), 1 deletion(-)

diff --git a/NEWS b/NEWS
index 2a40238d0a..4eadd9e51d 100644
--- a/NEWS
+++ b/NEWS
@@ -50,6 +50,8 @@ The following bugs are resolved with this release:
     saved registers
   [31429] build: Glibc failed to build with -march=x86-64-v3
   [31501] dynamic-link: _dl_tlsdesc_dynamic_xsavec may clobber %rbx
+  [31612] libc: arc4random fails to fallback to /dev/urandom if
+    getrandom is not present
   [31640] dynamic-link: POWER10 ld.so crashes in
     elf_machine_load_address with GCC 14
   [31676] Configuring with CC="gcc -march=x86-64-v3"
diff --git a/stdlib/arc4random.c b/stdlib/arc4random.c
index 3ae8fc1302..7818cb9cf6 100644
--- a/stdlib/arc4random.c
+++ b/stdlib/arc4random.c
@@ -51,7 +51,7 @@ __arc4random_buf (void *p, size_t n)
 	  n -= l;
 	  continue; /* Interrupted by a signal; keep going.  */
 	}
-      else if (l == -ENOSYS)
+      else if (l < 0 && errno == ENOSYS)
 	break; /* No syscall, so fallback to /dev/urandom.  */
       arc4random_getrandom_failure ();
     }
-- 
2.45.2

