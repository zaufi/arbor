# Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries

export_exlib_phases src_unpack src_install

SUMMARY="Intel CILK Runtime Library"

LICENCES="GPL-2"
SLOT="$(ever major)"

MYOPTIONS=""

DEPENDENCIES+="
    build:
        sys-libs/libstdc++:${SLOT}
"

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/libcilkrts"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/libcilkrts"
fi
WORK="${WORKBASE}/build/$(exhost --target)/libcilkrts"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-multilib )

# NOTE(compnerd) libcilkrts does not have a testsuite
RESTRICT="test"

libcilkrts_src_unpack() {
    default
    edo mkdir -p "${WORK}"

    # TODO(compnerd) find a more elegant solution to this (potentially addressing one of the
    # upstream FIXMEs in the process)
    edo sed -e "s,toolexecdir=no,toolexecdir='\${libdir}',"         \
            -e "s,toolexeclibdir=no,toolexeclibdir='\${libdir}',"   \
            -i "${ECONF_SOURCE}/configure"

    edo sed \
        -e "/cilkincludedir =/ccilkincludedir = /usr/$(exhost --build)/lib/gcc/\$(target_alias)/\$(gcc_version)/include/cilk" \
        -i "${ECONF_SOURCE}/Makefile.in"

    edo chmod +x "${ECONF_SOURCE}/configure"
}

libcilkrts_src_install() {
    default

    symlink_dynamic_libs ${PN}
    slot_dynamic_libs ${PN}
    slot_other_libs ${PN}.a ${PN}.la ${PN}.spec
}

