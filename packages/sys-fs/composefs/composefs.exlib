# Copyright 2024 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=containers release=v${PV} suffix=tar.xz ]
require meson

export_exlib_phases src_test

SUMMARY="A file system for mounting container images"
DESCRIPTION="
The composefs project combines several underlying Linux features to
provide a very flexible mechanism to support read-only mountable filesystem
trees, stacking on top of an underlying \"lower\" Linux filesystem."

LICENCES="
    Apache-2.0
    GPL-2
    || ( LGPL-2.1 LGPL-3 )
"
SLOT="0"
MYOPTIONS="
    fuse [[ description = [ Build support for FUSE ] ]]
"

DEPENDENCIES="
    build:
        dev-golang/go-md2man
        virtual/pkg-config
    build+run:
        dev-libs/openssl:=
        fuse? ( sys-fs/fuse:3[>=3.10.0] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dman=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    fuse
)

composefs_src_test() {
    # the check-random-fuse part of the check target doesn't work under sydbox
    # and needs [fuse] enabled
    edo meson test test-lcfs check-units check-checksums check-dump-filtered
}

