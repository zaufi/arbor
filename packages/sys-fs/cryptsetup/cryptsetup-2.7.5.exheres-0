# Copyright 2008 Stephen Bennett
# Copyright 2009 Mike Kelly
# Copyright 2011-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Userland utilities for dm-crypt"
HOMEPAGE="https://gitlab.com/${PN}/${PN}"
DOWNLOADS="mirror://kernel/linux/utils/${PN}/v$(ever range 1-2)/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    passwdqc [[ description = [ Password quality checking using passwdqc ] ]]
    pwquality [[ description = [ Password quality checking using libpwquality ] ]]
    sed-opal [[ description = [ Support For OPAL Self Encrypting Drives ] ]]
    ssh [[ description = [ Support for using remote keyfile through SSH protocol ] ]]
    (
        gcrypt [[ description = [ Use libgcrypt library as crypto backend ] ]]
        kernel [[ description = [ Use kernel as crypto backend, needs at least the kernel userspace
                                  crypto interface ] ]]
        nettle [[ description = [ Use nettle library as crypto backend ] ]]
        openssl [[ description = [ Use openssl library as crypto backend ] ]]
    ) [[ number-selected = exactly-one ]]
    openssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    ( passwdqc pwquality ) [[ number-selected = at-most-one ]]
    ( linguas: cs da de es fi fr id it ja ka nl pl pt_BR ro ru sr sv uk vi zh_CN )
"

# nss backend needs static lib
DEPENDENCIES="
    build:
        app-text/asciidoctor
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config[>=0.9.0]
        sed-opal? ( sys-kernel/linux-headers[>=6.4] )
    build+run:
        dev-libs/json-c:=
        dev-libs/popt[>=1.7]
        sys-apps/util-linux [[ note = [ cryptsetup needs libuuid ] ]]
        sys-fs/lvm2 [[ note = [ cryptsetup needs device-mapper from the LVM2 package ] ]]
        gcrypt? (
            dev-libs/libgcrypt[>=1.6.1][-caps(-)]
            dev-libs/libgpg-error
        )
        nettle? ( dev-libs/nettle:=[>=2.6] )
        openssl? (
            providers:libressl? (
                app-crypt/argon2
                dev-libs/libressl:=
            )
            providers:openssl? ( dev-libs/openssl:=[>=3.2] )
        )
        !openssl? (
            app-crypt/argon2
        )
        passwdqc? ( sys-auth/passwdqc )
        pwquality? ( dev-libs/libpwquality[>=1.0.0] )
        ssh? ( net-libs/libssh )
"

# static cryptsetup must be disabled because lvm2 can't be linked statically anymore.
# FIPS (requires libgcrypt or openssl) is probably not interesting for us, so hard disable it
src_configure() {
    local params=(
        --enable-asciidoc
        --enable-blkid
        --enable-cryptsetup
        --enable-dev-random
        --enable-external-tokens
        --enable-integritysetup
        --enable-keyring
        --enable-luks2-reencryption
        --enable-nls
        --enable-udev
        --enable-veritysetup
        --disable-fips
        --disable-fuzz-targets
        --disable-gcrypt-argon2
        --disable-internal-argon2
        --disable-selinux
        --disable-static-cryptsetup
        $(option_enable gcrypt gcrypt-pbkdf2)
        $(option_enable passwdqc)
        $(option_enable pwquality)
        $(option_enable sed-opal hw-opal)
        $(option_enable ssh ssh-token)
        $(option_with gcrypt libgcrypt-prefix /usr/$(exhost --target))
        --with-crypto_backend=$(
        if option gcrypt; then
            echo 'gcrypt'
        elif option kernel; then
            echo 'kernel'
        elif option nettle; then
            echo 'nettle'
        elif option openssl; then
            echo 'openssl'
        fi) \
        --with-tmpfilesdir=${SYSTEMDTMPFILESDIR}
    )

    if option openssl && option providers:openssl ; then
        params+=(
            # Ignored with openssl[>=3.2], it prefers openssl's implementation
            --disable-libargon2
        )
    else
        params+=(
            --enable-libargon2
        )
    fi

    econf "${params[@]}"
}

src_test() {
    esandbox allow /dev/random
    default
}

src_install() {
    default

    # remove empty directory
    option ssh || edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/${PN}
}

