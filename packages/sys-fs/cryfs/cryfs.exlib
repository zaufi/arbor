# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ suffix='tar.gz' release=${PV} ] cmake

export_exlib_phases src_prepare src_test

SUMMARY="Cryptographic filesystem for the cloud"
DESCRIPTION="
CryFS encrypts your files, so you can safely store them anywhere. It works
well together with cloud services like Dropbox, iCloud, OneDrive and others.
"
HOMEPAGE+=" https://www.cryfs.org/"

LICENCES="
    LGPL-3
    "
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-cpp/range[>=0.11.0]
        dev-lang/python:*
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.65.1]
        dev-libs/crypto++[>=8.6]
        dev-libs/fmt
        dev-libs/spdlog
        net-misc/curl
        sys-fs/fuse:0
        sys-libs/libgomp:=
    test:
        dev-cpp/gtest
"

CMAKE_SOURCE=${WORKBASE}

DEFAULT_SRC_PREPARE_PATCHES+=( "${FILES}"/${PNV}-unbundle-libs.patch )

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Upstream only installs stuff with CMAKE_BUILD_TYPE=Release, so we hide
    # the default and set it accordingly.
    --hates=CMAKE_BUILD_TYPE
    -DCMAKE_BUILD_TYPE=Release
    -DDEPENDENCY_CONFIG=../cmake-utils/DependenciesFromLocalSystem.cmake
    -DCRYFS_UPDATE_CHECKS:BOOL=FALSE
    -DUSE_SYSTEM_LIBS:BOOL=TRUE
    -DUSE_WERROR:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

cryfs_src_prepare() {
    cmake_src_prepare

    # Use the prefixed tool during tests
    edo sed \
        -e "/find_program/s/ addr2line/ $(exhost --tool-prefix)addr2line/" \
        -i src/cpp-utils/CMakeLists.txt
}
cryfs_src_test() {
    # Upstream really doesn't provide a test target, the following is stolen
    # from .travisci/build_and_test.sh
    edo pushd "${WORK}"/test
    edo gitversion/gitversion-test
    edo cpp-utils/cpp-utils-test
    edo parallelaccessstore/parallelaccessstore-test
    edo blockstore/blockstore-test
    edo blobstore/blobstore-test
    edo cryfs/cryfs-test
    edo popd
}

