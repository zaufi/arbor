# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'graphviz-2.18.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require gitlab [ new_download_scheme=true ]
require python [ blacklist=2 with_opt=true multibuild=false ]

SUMMARY="Open Source Graph Visualization Software"
HOMEPAGE+=" https://www.${PN}.org"
DOWNLOADS="https://gitlab.com/api/v4/projects/4207231/packages/generic/${PN}-releases/${PV}/${PNV}.tar.xz"

LICENCES="EPL-1.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    pdf
    postscript [[ description = [ Support for PostScript utilizing Ghostscript ] ]]
    python
    svg [[ description = [ Support node shapes in svg output ] ]]
    webp [[ description = [ Support for the Webp image format ] ]]
    X
"

DEPENDENCIES="
    build:
        sys-devel/flex
        virtual/pkg-config[>=0.20]
        python? ( dev-lang/swig[python] )
    build+run:
        dev-libs/expat[>=2.0.0] [[ note = [ Required for HTML-like labels ] ]]
        dev-libs/glib:2[>=2.36.0]
        media-libs/freetype:2
        media-libs/libpng:=
        sys-devel/libtool[>=2.2]
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/pango[>=1.22.0]
        pdf? ( app-text/poppler[glib] )
        postscript? ( app-text/ghostscript )
        svg? ( gnome-desktop/librsvg:2 )
        webp? ( media-libs/libwebp:= )
        X? (
            x11-libs/libX11
            x11-libs/libXrender
        )
"

#TODO_DEPENDENCIES+="
#    build:
#        lua? ( dev-lang/swig[lua] )
#        perl? ( dev-lang/swig[perl] )
#        ruby? ( dev-lang/swig[ruby] )
#    build+run:
#        lua? ( dev-lang/lua:= )
#        perl? ( dev-lang/perl:= )
#        ruby? ( dev-lang/ruby:= )
#"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-cgraph-Remove-redundant-declaration-of-aghtmlstr.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --htmldir=/usr/share/doc/${PNVR}/html
    --pdfdir=/usr/share/doc/${PNVR}/pdf
    --enable-ltdl
    --enable-nls
    --disable-debug
    --disable-go
    --disable-ltdl-install
    --disable-man-pdfs
    --disable-python
    --disable-static
    --with-freetype2
    --with-jpeg
    --with-pangocairo
    --with-png
    --without-demos
    --without-devil
    --without-libgd
    # Deprecated crap
    --without-gdk
    --without-gdk-pixbuf
    --without-ming
    # Deprecated (see ChangeLog for 10.0.1)
    --without-gtk
    --without-gtkgl
    # Need to fix install path for these bindings
    --disable-{perl,ruby}
    # dev-lang/lua doesn't install a .so, -fPIC errors
    --disable-lua
    # No swig-bindings yet
    --disable-{guile,java,io,php,r,sharp,tcl}
    # Requires extra packages (gts, smyrna, etc.), and doesn't seem to work yet (cgraph)
    --without-{ann,cgraph,glade,gtkglext,gts,lasi,smyrna}
    # disable gvedit, requires Qt
    --without-qt
)

#TODO_DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
#    perl ruby
#)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'python python3'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'pdf poppler'
    'postscript ghostscript'
    'svg rsvg'
    'webp'
    'X x'
)

src_prepare() {
    # respect selected PYTHON_ABI
    export PYTHON3=${PYTHON}

    default
}

src_install() {
    default

    edo mv "${IMAGE}"/usr/share/{${PN}/*,doc/${PNVR}/}
    # Disabled language bindings leave stray directories, as do other things..
    [[ -d "${IMAGE}"/usr/share/doc/${PNV}/demo/{pathplan_data,} ]] && edo rmdir "${IMAGE}"/usr/share/doc/${PNV}/demo/{pathplan_data,}
    edo find "${IMAGE}"/usr/ -type d -empty -delete
}

pkg_postinst() {
    dot -c
}

